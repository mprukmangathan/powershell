if (Test-Path C:\opt\) {
   echo "opt directory already exist" >> c:\opt\install.log
} 
else { 
   mkdir C:/opt/metadata
}

echo "installing software initiated" >> c:\opt\install.log
$list_of_apps = @("chrome", "7zip", "sql", "temnos", "winscp", "winzip", "ngrok", "pulsesecure")
for($i = 0; $i -lt $list_of_apps.length; $i++){ 
   $appname = $list_of_apps[$i]
   $isexist = Test-Path C:\opt\metadata\$appname.txt
   if ($isexist -eq "True") { 
      echo "$appname already installed" >> c:\opt\install.log
   }
   else {
      New-Item C:\opt\metadata\$appname.txt 
      Start-Sleep -s 60
      echo "$appname is successfully installed" >> c:\opt\install.log
      if ($appname -eq "sql") {
         echo "restarting server" >> c:\opt\install.log
         Restart-Computer
      }   
   }
}
echo "installing software completed" >> c:\opt\install.log
get-date >> c:\opt\install.log
echo "-----------------------------" >> c:\opt\install.log
exit
