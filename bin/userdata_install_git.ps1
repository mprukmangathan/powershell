<script>
cd c:/
mkdir software
cd c:/software
echo [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 > download_git.ps1
echo $down = New-Object System.Net.WebClient >> download_git.ps1
echo $url = 'https://github.com/git-for-windows/git/releases/download/v2.27.0.windows.1/Git-2.27.0-64-bit.exe' >> download_git.ps1
echo $file = 'C:\software\git.exe' >> download_git.ps1
echo $down.DownloadFile($url,$file) >> download_git.ps1
echo $exec = New-Object -com shell.application >> download_git.ps1
echo exit >> download_git.ps1

powershell "& ""C:\software\download_git.ps1"""

@echo off

set installDir=%1
if [%1]==[] set installDir="C:\Program Files\Git"
set installDir=%installDir:"=%

(
    echo [Setup]
    echo Lang=default
    echo Dir=%installDir%
    echo Group=Git
    echo NoIcons=0
    echo SetupType=default
    echo Components=icons,ext\reg\shellhere,assoc,assoc_sh
    echo Tasks=
    echo PathOption=Cmd
    echo SSHOption=OpenSSH
    echo CRLFOption=CRLFAlways
    echo BashTerminalOption=ConHost
    echo PerformanceTweaksFSCache=Enabled
    echo UseCredentialManager=Enabled
    echo EnableSymlinks=Disabled
    echo EnableBuiltinDifftool=Disabled
) > config.inf

for /r %%f in (git*.exe) do (
    call set file="%%f"
    @echo on
    @echo %%f
    @echo off
)

if [%file%]==[] (
    @echo on
    @echo Error finding "git*.exe" install executable. File may not exist or is not named with the "git" prefix.
    exit /b 2
)

@echo on
@echo Installing..
@echo off
%file% /VERYSILENT /LOADINF="config.inf"
if errorlevel 1 (
    @echo on
    if %errorLevel% == 1 ( echo Error opening %file%. File may be corrupt. )
    if %errorLevel% == 2 ( echo Error reading %file%. May require elevated privileges. Run as administrator. )
    exit /b %errorlevel%
)
del config.inf

net session >nul 2>&1
if %errorLevel% == 0 (
    setx PATH "%PATH%;%installDir%/cmd;"
    cd c:/
    mkdir repo
    cd c:/repo
    git clone https://mprukmangathan@bitbucket.org/mprukmangathan/powershell.git
    powershell "& ""C:\repo\powershell\install.ps1"""
) else (
    @echo on
    echo SYSTEM PATH Environment Variable may not be set, may require elevated privileges. Run as administrator if it doesn't already exist.
    exit /b 0
)
</script>
